#!/bin/bash

# Redirect server logs to stdout
ln -sf /proc/1/fd/1 ./CoreKeeperServerLog.txt

# Launch the server
if [ "${DIRECT_CONNECT}" = 1 ]
then
    /home/steam/core-keeper-server/_launch.sh -world "0" -worldname "${WORLD_NAME}" -worldseed "${WORLD_SEED}" -gameid "${GAME_ID}" -datapath "" -maxplayers "${MAX_PLAYERS}" -worldmode "${WORLD_MODE}" -port "27015" -ip "${IP_BIND_ADDRESS}"
else
    /home/steam/core-keeper-server/_launch.sh -world "0" -worldname "${WORLD_NAME}" -worldseed "${WORLD_SEED}" -gameid "${GAME_ID}" -datapath "" -maxplayers "${MAX_PLAYERS}" -worldmode "${WORLD_MODE}"
fi
